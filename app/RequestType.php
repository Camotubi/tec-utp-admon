<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestType extends Model
{
    protected $table = 'request_types';
    
    public function requests()
    {
        return $this->hasMany('App\Request', 'request_type_id', 'id');
    }
}
