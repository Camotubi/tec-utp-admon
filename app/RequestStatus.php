<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestStatus extends Model
{
    protected $table = 'request_statuses';

    public function requests()
    {
        return $this->hasMany('App\Request', 'request_status_id', 'id');
    }
}
