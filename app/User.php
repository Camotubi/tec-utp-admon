<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'cedula', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'users';

    public function getFullNameAttribute()
    {
	    return "{$this->first_name} {$this->last_name}";
    }

    public function roles()
    {
        return $this->belongsToMany('App\UserRole', 'users_user_roles', 'user_id', 'user_role_id');
    }

    public function requestsBy()
    {
        return $this->hasMany('App\FormRequest', 'requester_id', 'id');
    }

    public function requestsTo()
    {
        return $this->hasMany('App\FormRequest', 'incharge_id', 'id');
    }
}
