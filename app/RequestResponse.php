<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestResponse extends Model
{
	public function request(){
		return $this->belongsTo('App\FormRequest', 'form_request_id');
	}
}
