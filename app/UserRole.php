<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'user_roles';

    public function users()
    {
        return $this->belongsToMany('App\User', 'users_user_roles', 'user_role_id', 'user_id');
    }
}
