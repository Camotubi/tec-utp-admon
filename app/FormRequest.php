<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormRequest extends Model
{
    protected $table = 'requests';

    public function requester()
    {
        return $this->belongsTo('App\User', 'requester_id');
    }

    public function incharge()
    {
        return $this->belongsTo('App\User', 'incharge_id');
    }

    public function status()
    {
        return $this->belongsTo('App\RequestStatus', 'request_status_id');
    }

    public function type()
    {
        return $this->belongsTo('App\RequestType', 'request_type_id');
    }

    public function response()
    {
        return $this->hasOne('App\RequestResponse');
    }
}
