<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function show() {

		$requestsRequested = FormRequest::where('requester_id', Auth::id())
			->orderBy('updated_at')
			->get();
		$requestsInCharge = FormRequest::where('incharge_id', Auth::id())
			->orderBy('updated_at')
			->get();

		if(Auth::user()->roles()->where('user_role_id',2)->count()) {

			return view('funcionario_dashboard', ['requestsRequested' => $requestsRequested]);
		} else {
			return view('admon_dashboard', ['requestsRequested' => $requestsRequested,
				'requestsInCharge' => $requestsInCharge
				]);
		}
	}
}
