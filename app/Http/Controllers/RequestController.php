<?php

namespace App\Http\Controllers;

use Validator;
use App\RequestResponse;
use App\FormRequest;
use App\User;
use App\RequestType;
use App\RequestStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class RequestController extends Controller
{
	public function showViaticoForm()
	{
		return view('forms_create.viatico');
	}

	public function showServProfForm()
	{
		return view('forms_create.pago_por_servicio_profecional');
	}

	public function showSueldosForm()
	{
		return view('forms_create.sueldosDevengados');
	}

	public function showHorarioForm()
	{
		return view('forms_create.certificacionHorario');
	}

	public function showTrabajoForm()
	{
		return view('forms_create.certificacionTrabajo');
	}

	public function showDevolucionForm()
	{
		return view('forms_create.devolucion');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function indexUserRequests()
	{
		$requests = FormRequest::where('requester_id', Auth::id())
			->orderBy('updated_at')
			->get();
		return view('funcionario_dashboard', [
			'requests' => $requests,
			'showActions' => true,
			'showSolicitante' => false,
			'showCedula' => false,
		]);
	}
	public function indexAssignedRequests()
	{
		$requestsGeneral = FormRequest::Where('incharge_id', Auth::id())
			->orderBy('updated_at')->get();
		$requestsRequested = FormRequest::where('requester_id', Auth::id());
		$requestsInCharge = FormRequest::where('incharge_id', Auth::id());
		return view('forms_index', ['requests' => $requestsGeneral, 
			'requestsRequested' => $requestsRequested, 
			'requestsInCharge' => $requestsInCharge]);	    
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$formRequest = new FormRequest;
		$formRequest->requester()->associate(Auth::id());
		$type = $request->input('requestTypeId');
		switch ($type) {
		case 'Viatico':
			$formRequest->type()->associate(1);
			$validatorLocal = [
				'name' => 'required|string',
				'cedula' => 'required|string',
				'role' => 'required|string',
				'home_address' => 'required|string',
				'add_observations' => 'required|string',
				'objective' => ' required|string',	
				'viaticoType' => 'required|string',
				'place' => 'required|string',
				'checkOutDate' => 'required|date',
				'checkOutTime' => 'required|string',
				'checkInDate' => 'required|date',
				'checkInTime' => 'required|string',
				'transportation' => 'required|string',
			];
			$validatorInternational = array_merge(
				$validatorLocal,
				[
					'permisoPresidencia' => 'required|file|mimes:jpeg,bmp,png,gif,svg,pdf',
					'programaEvento' => 'required|file|mimes:jpeg,bmp,png,gif,svg,pdf',
					'rubrosOrganizacion' => 'required|file|mimes:jpeg,bmp,png,gif,svg,pdf',
					'aprobacionRectoria' => 'required|file|mimes:jpeg,bmp,png,gif,svg,pdf',
				]);


			if ($request->input('viaticoType') == "Internacional") {
				$validator = Validator::make($request->all(), $validatorInternational );
				if ($validator->fails()) {
					return redirect('viaticos/create')
						->withErrors($validator)
						->withInput()
						->with('error_msg', 'Hay campos en blanco');
				}
				$this->assignInCharge($formRequest, 3);
				$permisoPresi = Storage::disk('public')->putFile('viaticos', $request->file('permisoPresidencia'));
				$progEven = Storage::disk('public')->putFile('viaticos', $request->file('programaEvento'));
				$rubOrg = Storage::disk('public')->putFile('viaticos', $request->file('rubrosOrganizacion'));
				$aprobRect = Storage::disk('public')->putFile('viaticos', $request->file('aprobacionRectoria'));

				
			}else {
				$validator = Validator::make($request->all(), $validatorLocal);
				if ($validator->fails()) {
					return redirect('viaticos/create')
						->withErrors($validator)
						->withInput()
						->with('error_msg', 'Hay campos en blanco');
				}
				$this->assignInCharge($formRequest, 4);
			}
			break;
		case 'Servicio Profesional':
			$formRequest->type()->associate(5);
			$this->assignInCharge($formRequest, 4);
			break;
		case 'Devolución por Descuento':
			$formRequest->type()->associate(4);
			$this->assignInCharge($formRequest, 4);
			break;
		case 'Sueldos Devengados':
			$formRequest->type()->associate(6);
			$this->assignInCharge($formRequest, 1);
			break;
		case 'Certificación de Horario':
			$formRequest->type()->associate(2);
			$this->assignInCharge($formRequest, 1);
			break;
		case 'Certificación de Trabajo':
			$formRequest->type()->associate(3);
			$this->assignInCharge($formRequest, 1);
			break;
		}
		$formRequest->status()->associate(1);
		if($request->has('viaticoType') && $request->input('viaticoType') === "Internacional")
		{
			$formRequest->body = json_encode(
				array_merge(
					$request->input(),
					[
						'permisoPresidencia' =>$permisoPresi,
						'programaEvento' => $progEven,
						'rubrosOrganizacion' =>$rubOrg,
						'aprobacionRectoria' =>$aprobRect,
					]
				)
			);
		} else {
			$formRequest->body = json_encode($request->input());
		}
		$formRequest->save();
		return redirect('/assigned/requests');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\FormRequest  $formRequest
	 * @return \Illuminate\Http\Response
	 * FormRequest $formRequest
	 */
	public function show($id)
	{
		try {
			$formRequest = FormRequest::findOrFail($id);
			$body = json_decode($formRequest->body);
			switch ($formRequest->request_type_id) {
			case '1':
				return view('forms_show.viatico', ['formRequest' => $formRequest, 'requestStatus' => $formRequest->request_status_id, 'body' => $body]);
				break;
			case '2':
				return view('forms_show.certificacionHorario', ['formRequest' => $formRequest, 'requestStatus' => $formRequest->request_status_id, 'body' => $body]);
				break;
			case '3':
				return view('forms_show.certificacionTrabajo', ['formRequest' => $formRequest, 'requestStatus' => $formRequest->request_status_id, 'body' => $body]);
				break;
			case '4':
				return view('forms_show.devolucion', ['formRequest' => $formRequest, 'requestStatus' => $formRequest->request_status_id, 'body' => $body]);
				break;
			case '5':
				return view('forms_show.pago_por_servicio', ['formRequest' => $formRequest, 'requestStatus' => $formRequest->request_status_id, 'body' => $body]);
				break;
			case '6':
				return view('forms_show.sueldosDevengados', ['formRequest' => $formRequest, 'requestStatus' => $formRequest->request_status_id, 'body' => $body]);
				break;
			}
		}
		catch(ModelNotFoundException $e)
		{
			return redirect()->back()->with('error', 'Solicitud no encontrada');
		}

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\FormRequest  $formRequest
	 * @return \Illuminate\Http\Response
	 */
	public function edit(FormRequest $formRequest)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\FormRequest  $formRequest
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$formRequest = FormRequest::find($id);
		if(!$request->has('action') && $request->input('action') !== "respond") 
		{
			$requestStatus = RequestStatus::find($request->input('requestStatusId'));
			$formRequest->status()->associate($requestStatus);
			$formRequest->save();
			if ($requestStatus->id == 2) {
				return redirect('/assigned/requests')->with('success_msg', 'Solicitud aprobada');

			}elseif ($requestStatus->id == 3) {
				return redirect('/assigned/requests')->with('success_msg', 'Solicitud rechazada');

			}elseif ($requestStatus->id == 4) {
				return redirect('/assigned/requests')->with('success_msg', 'Solicitud enviada al Ministerio de Finanzas');
			}

		} else {
			$file = $request->file('documento');
			$file = Storage::disk('public')->putFile('responses', $file);
			$obs = $request->input('obs');
			$response = new RequestResponse();
			$response->documento=$file;
			$response->obs = $obs;
			$response->request()->associate($formRequest);
			$response->save();
			$formRequest->request_status_id = 4;
			$formRequest->save();
			return redirect('/assigned/requests')->with('success_msg', 'Respuesta enviada correctamente.');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\FormRequest  $formRequest
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(FormRequest $formRequest)
	{
		//
	}

	public function assignInCharge(FormRequest $formRequest, int $user_role_id)
	{
		$formRequest
			->incharge()
			->associate(DB::table('users_user_roles')
			->where([
				['user_role_id', $user_role_id],
				['user_id', '<>', Auth::id()]
			])->inRandomOrder()->first()->user_id
		);

	}


	public function showDenyRequest(int $id) 
	{
		return view('form_deny', ['id' => $id]);
	}

	public function denyRequest(Request $request, int $id)
	{
		try{
			$formRequest = FormRequest::findOrFail($id);
			$validator = Validator::make($request->all(), ['reason' => 'string|optional'] );
			$formRequest->request_status_id = 3;
			$formRequest->save();
			$response = new RequestResponse();
			$response->request()->associate($formRequest);
			$response->obs = $request->input('reason');
			$response->save();
			return redirect('/assigned/requests')->with('success_msg', 'Solicitud Rechazada');

		}
		catch(ModelNotFoundException $e)
		{
			return redirect()->back()->with('error', 'Solicitud no encontrada');
		}
	}
}
