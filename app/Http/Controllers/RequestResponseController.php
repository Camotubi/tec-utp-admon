<?php

namespace App\Http\Controllers;

use App\RequestResponse;
use Illuminate\Http\Request;

class RequestResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequestResponse  $requestResponse
     * @return \Illuminate\Http\Response
     */
    public function show(RequestResponse $requestResponse)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequestResponse  $requestResponse
     * @return \Illuminate\Http\Response
     */
    public function edit(RequestResponse $requestResponse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequestResponse  $requestResponse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequestResponse $requestResponse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequestResponse  $requestResponse
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequestResponse $requestResponse)
    {
        //
    }
}
