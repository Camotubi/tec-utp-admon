<?php

use Faker\Generator as Faker;

$factory->define(App\RequestType::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->text
    ];
});
