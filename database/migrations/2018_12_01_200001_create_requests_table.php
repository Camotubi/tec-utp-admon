<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('requester_id')->unsigned();
            $table->integer('incharge_id')->unsigned();
            $table->foreign('requester_id')->references('id')->on('users');
            $table->foreign('incharge_id')->references('id')->on('users');
            $table->timestamps();
            $table->integer('request_status_id')->unsigned();
            $table->integer('request_type_id')->unsigned();
            $table->foreign('request_status_id')->references('id')->on('request_statuses');
            $table->foreign('request_type_id')->references('id')->on('request_types');
            $table->json('body');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
