<?php

use Illuminate\Database\Seeder;
use App\RequestType;

class RequestTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RequestType::insert([
            ['id' => 1, 'name' => 'Viatico', 'description' => 'Lorem'],
            ['id' => 2, 'name' => 'Certificación de Horario', 'description' => 'Ipsum'],
            ['id' => 3, 'name' => 'Certificación de Trabajo', 'description' => 'Algo'],
            ['id' => 4, 'name' => 'Devolución por descuentos ', 'description' => 'Ehhh'],
            ['id' => 5, 'name' => 'Pago de Servicio Profesional', 'description' => 'Certificación de servicio realizado y de recibido conforme'],
            ['id' => 6, 'name' => 'Sueldos Devengados', 'description' => 'Emisión de documento que detalla los sueldos devengados en un período determinado a
            solicitud del funcionario para presentar la declaración de impuestos sobre la renta ante el Ministerio de Economía y Finanzas.']
        ]);


    }
}
