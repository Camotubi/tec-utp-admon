<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	App\User::create([
		'first_name' => 'admin',
		'last_name' => 'admin',
		'cedula' => 'lacedula',
		'email' => 'admin@admin.com',
		'password' => bcrypt('1234'), // secret
		'remember_token' => str_random(10)
	]);
        factory(App\User::class, 3)->create();
        factory(App\User::class, 3)->create();
        factory(App\User::class, 3)->create();
        factory(App\User::class, 3)->create();
    }
}
