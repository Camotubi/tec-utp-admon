<?php

use Illuminate\Database\Seeder;

class UserUserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_user_roles')->insert([
            ['id' => 1, 'user_id' => 1, 'user_role_id' => 1],
            ['id' => 2, 'user_id' => 1, 'user_role_id' => 2],
            ['id' => 3, 'user_id' => 2, 'user_role_id' => 1],
            ['id' => 4, 'user_id' => 2, 'user_role_id' => 2],
            ['id' => 5, 'user_id' => 3, 'user_role_id' => 3],
            ['id' => 6, 'user_id' => 3, 'user_role_id' => 2],
            ['id' => 7, 'user_id' => 4, 'user_role_id' => 3],
            ['id' => 8, 'user_id' => 4, 'user_role_id' => 2],
            ['id' => 9, 'user_id' => 5, 'user_role_id' => 4],
            ['id' => 10, 'user_id' => 5, 'user_role_id' => 2],
            ['id' => 11, 'user_id' => 6, 'user_role_id' => 4],
            ['id' => 12, 'user_id' => 6, 'user_role_id' => 2]
        ]);
    }
}
