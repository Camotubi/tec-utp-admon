<?php

use Illuminate\Database\Seeder;
use App\UserRole;
class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	UserRole::insert([
		['id' => 1, 'name' => 'Recursos Humanos', 'description' => 'Lorem'],
		['id' => 2, 'name' => 'Funcionario', 'description' => 'Ipsum'],
		['id' => 3, 'name' => 'Vicerrectoria', 'description' => 'Algo'],
		['id' => 4, 'name' => 'Finanzas', 'description' => 'Ehhh']
	]);
    }
}
