<?php

use Illuminate\Database\Seeder;
use App\RequestStatus;

class RequestStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RequestStatus::insert([
            ['id' => 1, 'name' => 'Enviada', 'description' => '-'],
            ['id' => 2, 'name' => 'En proceso', 'description' => '-'],
            ['id' => 3, 'name' => 'Rechazada', 'description' => '-'],
            ['id' => 4, 'name' => 'Respondida', 'description' => '-'],
        ]);


    }
}
