<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/my/requests');
})->middleware('auth');

Auth::routes();
Route::get('/my/requests', 'RequestController@indexUserRequests');
Route::get('/assigned/requests', 'RequestController@indexAssignedRequests');
Route::resources([
    'user' => 'UserController',
    'request' => 'RequestController'
]);
Route::get('/deny/request/{id}','RequestController@showDenyRequest');
Route::post('/deny/request/{id}','RequestController@denyRequest');
#ESTA NO SE DE DONDE SALIO -->Route::get('/misionoficial', 'RequestController@showMisionForm');
Route::get('/viaticos/create', 'RequestController@showViaticoForm');
Route::get('/cert-horarios/create','RequestController@showHorarioForm');
Route::get('/cert-sueldos-devengados/create', 'RequestController@showSueldosForm');
Route::get('/cert-trabajo/create','RequestController@showTrabajoForm');
Route::get('/pago-serv-pro/create','RequestController@showServProfForm');
Route::get('/devol-desc-mas/create','RequestController@showDevolucionForm');

Route::get('/dashboard','DashboardController@show');
