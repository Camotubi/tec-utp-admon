<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>{{ config('app.name', 'Laravel') }}</title>

		<!-- Scripts -->

		<!-- Fonts -->
		<link rel="dns-prefetch" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

		<!-- Styles -->
		<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	</head>
	<body>
		<div class="container-fluid p-0">
			<div class="row">
				<div class="container-fluid p-0">
					@include('components.navbar')
				</div>
			</div>
			@if(session('error_msg'))
				<div class="alert alert-danger">
					{{session('error_msg')}}
				</div>
			@endif
			@if(session('success_msg'))
				<div class="alert alert-success">
					{{session('success_msg')}}
				</div>
			@endif
		<div class="row">
			<div class="container">
				@yield('content')
			</div>
		</div>
		</div>
		<script src="{{ asset('js/app.js') }}" defer></script>
		@yield('script')
	</body>
</html>
