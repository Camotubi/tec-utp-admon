@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<h2>Devolución por descuentos</h2>
			<p class="lead">
				Dentro de los trámites y procedimientos administrativos de desempeño interno cotidiano que los administrativos realizan en la Universidad Tecnológica de Panamá<br />
				<small class="text-muted">Consulta sobre sueldos desvengados en: <a href="http://www.utp.ac.pa/documentacion-requerida-para-devoluciones-por-descuentos-de-mas" target="blank">Universidad Tecnológica de Panamá - Trámites y Procedimientos</a></small>
			</p>

			<div class="container">
				<div class="alert alert-warning">
					<h4>Advertencia</h4>
					Recuerde verificar la información antes de enviarla.
				</div>
			</div>

			<hr />

			<div class="row">
				<div class="col-sm-8">

					<h4 class="page-header">Complete su solicitud</h4>
					<form role="form" method="POST" action="/request">
						@csrf
						<div class="form-row">
							<div class="col form-group float-label-control">
								<label for="">Institución:</label>
								<input type="text" name="institucion" class="form-control" placeholder="Roy Tuñón">
							</div>
							<div class="col form-group float-label-control">
								<label for="">Número:</label>
								<input type="text" name="numero" class="form-control" placeholder="8-123-456">
							</div>
						</div>
						<div class="form-row">
							<div class="col form-group float-label-control">
								<label for="">Fecha:</label>
								<input type="date" name="fecha" class="form-control" placeholder="Roy Tuñón">
							</div>
							<div class="col form-group float-label-control">
								<label for="">Hora:</label>
								<input type="time" name="hora" class="form-control" placeholder="Roy Tuñón">
							</div>
						</div>
						<div class="form-row">
							<div class="col form-group float-label-control">
								<div class="form-row">
									<label for="">Tipo de Fondo:</label>
								</div>
								<div class="form">
									<label for="" class="radio">
										<input type="radio-inline" name="fundType" class="form-control" value="Tesoro Nacional" id="fondoNac" checked> Tesoro Nacional
									</label>
									<label for="" class="radio">
										<input type="radio-inline" name="fundType" class="form-control" value="Fondo Institucional" id="fondoInt"> Fondo Institucional  
									</label>
							</div>
						</div>
					<div class="col form-group float-label-control" id="divNombreFondo">
					</div>
					<div>
					<hr>
					<div class="form-group float-label-control">
						<h2>A FAVOR DE</h2>
					</div>
					<div class="form-row">
						<div class="col form-group float-label-control">
							<label for="">Nombre:</label>
							<input type="name" name="nombre" class="form-control" placeholder="Roy Tuñón">
						</div>
						<div class="col form-group float-label-control">
							<label for="">Cédula o RUC:</label>
							<input type="text" name="cedula/ruc" class="form-control" placeholder="Roy Tuñón">
						</div>
					</div>
					<div class="form-row">
						<div class="col form-group float-label-control">
							<label for="">Código de Proveedor:</label>
							<input type="text" name="codigoProveedor" class="form-control" placeholder="Roy Tuñón">
						</div>
						<div class="col form-group float-label-control">
							<label for="">Fecha de elaboración:</label>
							<input type="date" name="fechaElaboración" class="form-control" placeholder="Roy Tuñón">
						</div>
					</div>
					<!--Pendiente: Firma del representante legal o su equivalente-->
					<div class="form-row">
						<div class="col form-group float-label-control">
							<label for="">Cédula:</label>
							<input type="text" name="cedulaRepresentante" class="form-control" placeholder="Roy Tuñón">
						</div>
						<div class=" col form-group float-label-control">
							<label for="">Teléfono:</label>
							<input type="number" name="telefonoRepresentante" class="form-control" placeholder="Roy Tuñón">
						</div>
						<div>
						<div class="form-row">
							<div class="col form-group float-label-control">
								<label for="">Endosar a nombre de:</label>
								<input type="name" name="endosoNombre" class="form-control" placeholder="Roy Tuñón">
							</div>
							<div class="col form-group float-label-control">
								<label for="">Fecha del Endoso:</label>
								<input type="date" name="endosoFecha" class="form-control" placeholder="Roy Tuñón">
							</div>
						</div>
						<div class="form-row">
							<div class="col form-group float-label-control">
								<label for="">Cédula o RUC del adjudicatario del endoso:</label>
								<input type="text" name="endosoCedulaRuc" class="form-control" placeholder="Roy Tuñón">
							</div>
						</div>
						<!--Pendiente: Firma del que cede los derechos al adjudicatario-->
						<hr>
						<div class="col form-group float-label-control">
							<h2>DETALLE SUSTENTADOR DEL COBRO</h2>
						</div>
						<div class="form-row">
							<div class="col form-group float-label-control">
								<label for="">Contrato Número:</label>
								<input type="number" name="numeroContrato" class="form-control" placeholder="8-123-456">
							</div>
							<div class="col form-group float-label-control">
								<label for="">Adenda Número:</label>
								<input type="number" name="numeroAdenda" class="form-control" placeholder="8-123-456">
							</div>
						</div>
						<div class="form-row">
							<div class="col form-group float-label-control">
								<label for="">Convenio Número:</label>
								<input type="number" name="numeroConvenio" class="form-control" placeholder="8-123-456">
							</div>
							<div class="col form-group float-label-control">
								<label for="">Resolución Número:</label>
								<input type="number" name="numeroResolucion" class="form-control" placeholder="8-123-456">
							</div>
						</div>
						<div class="form-row">
							<div class="col form-group float-label-control">
								<label for="">Orden de Compra Número:</label>
								<input type="number" name="numeroOrden" class="form-control" placeholder="8-123-456">
							</div>
							<div class="col form-group float-label-control">
								<label for="">Factura Número:</label>
								<input type="number" name="numeroFactura" class="form-control" placeholder="8-123-456">
							</div>
						</div>
						<div class="form-row">
							<div class="col form-group float-label-control">
								<label for="">Descripción de bien o servicio brindado:</label>
								<textarea class="form-control" name="descripcion" placeholder="Participación en el foro: UTP EMPRENDE en el hotel Hilton."
									rows="10"></textarea>
							</div>
						</div>
						<hr>
						<div class="form-group float-label-control">
							<h2>VALORES EN BALBOAS</h2>
						</div>
						<div class="form-group float-label-control">
							<label for="">Valor Bruto:</label>
							<input type="number" name="valorBruto" class="form-control" placeholder="8-123-456">
						</div>
						<div class="form-group float-label-control">
							<p>Menos</p>
						</div>
						<div class="form-group float-label-control row">
							<label for="">Retención x Garantía (<div class="col-xs-1"><input type="number" name="retenPorGarantiaPorcentaje" class="form-control" placeholder="8-123-456"></div>%):</label>
							<input type="number" name="retenPorGarantia" class="form-control" placeholder="8-123-456">
						</div>
						<div class="form-group row">
							<label for="">Anticipo (<div class="col-xs-1"><input type="number" name="anticipoPorcentaje" class="form-control" placeholder="8-123-456"></div>%):</label>
							<input type="number" name="anticipo" class="form-control" placeholder="8-123-456">
						</div>
						<div class="form-group float-label-control">
							<p>Más</p>
						</div>
						<div class="form-group float-label-control">
							<label for="">ITBMS:</label>
							<input type="number" name="itbms" class="form-control" placeholder="8-123-456">
						</div>
						<div class="form-group float-label-control">
							<label for="">Sume 911:</label>
							<input type="number" name="sume911" class="form-control" placeholder="8-123-456">
						</div>
						<div class="form-group float-label-control">
							<label for="">Impuesto Selectivo al Consumo:</label>
							<input type="number" name="impSelectConsumo" class="form-control" placeholder="8-123-456">
						</div>
						<div class="form-group float-label-control">
							<p>Menos</p>
						</div>
						<div class="form-group float-label-control row">
							<label for="">Retención del <div class="col-xs-1"><input type="number" name="retenPorcentajeItbms" class="form-control" placeholder="8-123-456"></div>% del ITBMS:</label>
							<input type="number" name="retenItbms" class="form-control" placeholder="8-123-456">
						</div>
						<div class="form-group float-label-control">
							<label for="">Valor Total a Cobrar en letras:</label>
							<input type="text" name="valorTotalLetras" class="form-control" placeholder="8-123-456">
						</div>
						<div class="form-group float-label-control">
							<label for="">Valor Total a Cobrar en número:</label>
							<input type="number" name="valorTotalNumero" class="form-control" placeholder="8-123-456">
						</div>
						<hr>
						<div class="form-group float-label-control">
							<h2>FIRMAS Y SELLOS POR REGISTROS Y AUTORIZACIÓN INSTITUCIONAL</h2>
						</div>
						<!--Discutir si lo demas se debe agregar ya que son puros sellos-->
						<div>
							<input type="hidden" name="requestTypeId" value="Devolución por Descuento">
						</div>
						<div class="form-group float-label-control">
							<button class="btn btn-primary" type="submit">Enviar solicitud</button>
						</div>
					</form>


						</div>
						<div class="col-sm-4">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">
										Otras Solicitudes
									</h3>
								</div>
								<div class="panel-body">
									<ul>
										<li><a href="/create/sueldosDevengados">Sueldos Devengados</a></li>
										<li><a href="/create/viatico">Solicitud de Viático</a></li>
										<li><a href="/create/certificacionHorario">Certificación de Horario</a></li>
										<li><a href="/create/certificacionTrabajo">Certificación de Trabajo</a></li>
										<li><a href="/create/pagoServProf">Servicio Profesional</a></li>
									</ul>
								</div>
							</div>
						</div>
						</div>
						</div>
						</div>
					@endsection
					@section('script')
					<script>
						document.getElementById("fondoNac").addEventListener("click", removeFields);
					    document.getElementById("fondoInt").addEventListener("click", displayFields);
					
					    function displayField() {
					        document.getElementById("divNombreFondo").innerHTML = "<hr><label for=''>Nombre de Fondo:</label><input type='name' name='nombreFondo' class='form-control' placeholder='Roy Tuñón'>";
					    }
					
					    function removeField() { 
					        document.getElementById("divNombreFondo").innerHTML = "";
					    }
					
					</script>
				@endsection
