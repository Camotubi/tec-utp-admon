@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<h2>Certificación de Horarios</h2>
			<p class="lead">
				En la Certificación de horario de labores se detalla el horario y periodo en que laboró un docente.
				Una de las razones por las cuales los docentes solicitan esta certificación es para cumplir con uno de los requisitos del proceso de jubilación.<br />
				<small class="text-muted">Consulta sobre las certificaciones en: <a href="http://www.utp.ac.pa/certificaciones" target="blank">Universidad Tecnológica de Panamá - Certificaciones</a></small>
			</p>

			<div class="container">
				<div>
					<div class="alert alert-warning" role="alert">
						Recuerde verificar la información antes de enviarla.
					</div>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-sm-8">
				<h4 class="page-header">Complete su solicitud</h4>
				<form class = "form" role="form" method="POST" action="/request">
					@csrf
					<div class="form-group float-label-control">
						<label for="">Nombre</label>
						<input type="name" name="nombre" class="form-control" placeholder="Roy Tuñón">
					</div>
					<div class="form-group float-label-control">
						<label for="">Cédula</label>
						<input type="text" class="form-control" placeholder="8-123-456" name="cedula">
					</div>
					<div class="form-group float-label-control">
						<label for="">Por favor detalle el periodo y el horario en el que laboro:</label>
						<textarea name="detallesHorarioPeriodo" rows="4" cols="50"></textarea>
					</div>
					<div>
						<input type="hidden" name="requestTypeId" value="Certificación de Horario">
					</div>
					<div class="form-group float-label-control">
						<button class="btn btn-primary" type="submit">Enviar solicitud</button>
					</div>

				</form>


			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							Otras Solicitudes
						</h3>
					</div>
					<div class="panel-body">
						<ul>
							<li><a href="/create/sueldosDevengados">Sueldos Devengados</a></li>
							<li><a href="/create/viatico">Solicitud de Viático</a></li>
							<li><a href="/create/certificacionTrabajo">Certificación de Trabajo</a></li>
							<li><a href="/create/devolucion">Devolución por Descuento</a></li>
							<li><a href="/create/pagoServProf">Servicio Profesional</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
