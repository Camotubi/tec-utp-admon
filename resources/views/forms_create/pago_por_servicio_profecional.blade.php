@extends('layouts.app')
@section('content')
	<div class="container">
    <div class="row">
        <h2>Pago de Servicio Profesional</h2>
        <p class="lead">
            Certificación de servicio realizado y de recibido conforme<br />
            <small class="text-muted">Consulta sobre documentación requerida para pago por servicios profesionales <a href="http://www.utp.ac.pa/documentacion-requerida-para-pago-por-servicios-profesionales" target="blank">Universidad Tecnológica de Panamá - Tŕamites y Procedimientos</a></small>
        </p>
    </div>
        <div class="alert alert-warning">
            <h4>Advertencia</h4>
           Recuerde verificar la información antes de enviarla.
        </div>

        <hr />

        <div class="row">
            <div class="col-sm-8">

                <h4 class="page-header">Complete su solicitud</h4>
                <form role="form" method="POST" action="/request">
                    @csrf
					<div class="form-group float-label-control">
						<label for="">Nombre:</label>
						<input type="name" name="name" class="form-control" placeholder="Roy Tuñón">
					</div>
					<div class="form-group float-label-control">
						<label for="">Cédula:</label>
						<input type="text" name="cedula" class="form-control" placeholder="8-123-456">
					</div>
					<div class="form-group float-label-control">
						<label for="">Cargo que desempeña:</label>
						<input type="text" name="role" class="form-control" placeholder="Profesor de ciencias físicas">
					</div>
					<div class="form-group float-label-control">
						<label for="">Lugar de Residencia:</label>
						<input type="text" name="home_address" class="form-control" placeholder="Brisas del Golf">
					</div>
					<div class="form-group float-label-control">
						<label for="">Observaciones adicionales:</label>
						<textarea class="form-control" name="add_observations" placeholder="Calle tercera, después de los bomberos." rows="1"></textarea>
					</div>
					<div class="form-group float-label-control">
						<label for="">Servicio ofrecido:</label>
						<textarea class="form-control" name="objective" placeholder="Taller de emprendimiento"
						      rows="1"></textarea>
					</div>
					
					<div class="form-group float-label-control">
						<label for="">Lugar del servicio:</label>
						<input type="text" name="place" class="form-control" placeholder="Escuela Superior">
					</div>
					<div class="form-group float-label-control">
						<label for="">Fecha del servicio:</label>
						<input type="date" name="checkInDate" class="form-control">
					</div>
					<div class="form-group float-label-control">
						<label for="">Hora de llegada:</label>
						<input type="time" name="checkIntTime" class="form-control">
					</div>
					<div class="form-group float-label-control">
						<label for="">Hora de salida:</label>
						<input type="time" name="checkOutDate" class="form-control">
                    </div>
                    <input type="hidden" name="requestTypeId" value="Servicio Profesional">
                    <div class="form-group float-label-control">
                    <button class="btn btn-primary" type="submit">Enviar solicitud</button>
                    </div>






                </form>


            </div>
            <div class="col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Otras Solicitudes
                        </h3>
                    </div>
                    <div class="panel-body">
                        <ul>
                            <li>Solicitud de Viático</li>
                            <li>Sueldos Devengados</li>
                            <li>Certificación de Horario</li>
                            <li>Certificación de Trabajo</li>
                            <li>Devolución por Descuento</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
