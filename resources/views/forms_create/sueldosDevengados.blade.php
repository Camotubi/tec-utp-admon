@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			 <div class="container">
			<h2>Solicitudes de Sueldos Devengados</h2>
			<p class="lead">
				Certificación de servicio realizado y de recibido conforme
			</p>
			<p><small class="text-muted">Consulta sobre los viáticos en: <a href="http://www.utp.ac.pa/viaticos" target="blank">Universidad Tecnológica de Panamá - Viáticos</a></small></p>

			</div>
			<div class="container">
				<div class="alert alert-warning">
					<h4>Advertencia</h4>
					Recuerde verificar la información antes de enviarla.
				</div>
			</div>
		</div> 
		<hr >

		<div class="row">
			<div class="col-sm-8">

				<h4 class="page-header">Complete su solicitud</h4>
				<form role="form" method="POST" action="/request">
					@csrf
					<div class="form-group float-label-control">
						<label for="">Nombre:</label>
						<input type="name" name="name" class="form-control" placeholder="Roy Tuñón">
					</div>
					<div class="form-group float-label-control">
						<label for="">Cédula:</label>
						<input type="text" name="cedula" class="form-control" placeholder="8-123-456">
					</div>
				<h5>Rango de solicitud</h5>
					<div class="form-group float-label-control">
						<label for="">Inicio:</label>
						<input type="date" name="CheckInDate" class="form-control" placeholder="2015">
					</div>
					<div class="form-group float-label-control">
							<label for="">Final:</label>
							<input type="date" name="CheckOutDate" class="form-control" placeholder="2015">
						</div>
					
                    <input type="hidden" name="requestTypeId" value="Sueldos Devengados">
						
					<div class="form-group float-label-control">
						<button class="btn btn-primary" type="submit">Enviar solicitud</button>
					</div>
				</form>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							Otras Solicitudes
						</h3>
					</div>
					<div class="panel-body">
						<ul>
							<li><a href="/create/pagoServProf">Servicio Profesional</a></li>
							<li><a href="/create/sueldosDevengados">Sueldos Devengados</a></li>
							<li><a href="/create/certificacionHorario">Certificación de Horario</a></li>
							<li><a href="/create/certificacionTrabajo">Certificación de Trabajo</a></li>
							<li><a href="/create/devolucion">Devolución por Descuento</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
