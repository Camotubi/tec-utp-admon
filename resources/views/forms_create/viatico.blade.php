@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			 <div class="container">
			<h2>Solicitudes de Viáticos</h2>
			<p class="lead">
				Certificación de servicio realizado y de recibido conforme
			</p>
			<p><small class="text-muted">Consulta sobre los viáticos en: <a href="http://www.utp.ac.pa/viaticos" target="blank">Universidad Tecnológica de Panamá - Viáticos</a></small></p>

			</div>
			<div class="container">
				<div class="alert alert-warning">
					<h4>Advertencia</h4>
					Recuerde verificar la información antes de enviarla.
				</div>
			</div>
		</div> 
		<hr >

		<div class="row">
			<div class="col-sm-8">

				<h4 class="page-header">Complete su solicitud</h4>
				<form role="form" method="POST" action="/request" enctype="multipart/form-data">
					@csrf
					<div class="form-group float-label-control">
						<label for="">Nombre:</label>
						<input type="name" name="name" class="form-control" placeholder="Roy Tuñón">
					</div>
					<div class="form-group float-label-control">
						<label for="">Cédula:</label>
						<input type="text" name="cedula" class="form-control" placeholder="8-123-456">
					</div>
					<div class="form-group float-label-control">
						<label for="">Cargo que desempeña:</label>
						<input type="text" name="role" class="form-control" placeholder="Vicedecano de FISC">
					</div>
					<div class="form-group float-label-control">
						<label for="">Lugar de Residencia:</label>
						<input type="text" name="home_address" class="form-control" placeholder="Brisas del Golf">
					</div>
					<div class="form-group float-label-control">
						<label for="">Observaciones adicionales:</label>
						<textarea class="form-control" name="add_observations" placeholder="Calle tercera, después de los bomberos." rows="1"></textarea>
					</div>
					<div class="form-group float-label-control">
						<label for="">Trabajo o misión a realizar:</label>
						<textarea class="form-control" name="objective" placeholder="Participación en el foro: UTP EMPRENDE en el hotel Hilton."
						      rows="1"></textarea>
					</div>
					<div class="form-group float-label-control">
						<label for="">Tipo de viaje:</label> <br>
						<label for="" class="radio-inline">
							<input type="radio" name="viaticoType" class="form-control" value="Local" id="loc" checked> Local
						</label>
						<label for="" class="radio-inline">
							<input type="radio" name="viaticoType" class="form-control" value="Internacional" id="int"> Internacional    
						</label>
					</div>
					<div id="divPermisoPresi" class="form-group float-label-control">
					</div>
					<div id="divProgramaEvento" class="form-group float-label-control">
					</div>
					<div id="divRubrosOrganización" class="form-group float-label-control">
					</div>
					<div id="divAprobaciónRectoria" class="form-group float-label-control">
					</div>
					<div class="form-group float-label-control">
						<label for="">Lugar de la misión:</label>
						<input type="text" name="place" class="form-control" placeholder="UTP - Centro regional Azuero">
					</div>
					<div class="form-group float-label-control">
						<label for="">Fecha de salida:</label>
						<input type="date" name="checkOutDate" class="form-control">
					</div>
					<div class="form-group float-label-control">
						<label for="">Hora de salida:</label>
						<input type="time" name="checkOutTime" class="form-control">
					</div>
					<div class="form-group float-label-control">
						<label for="">Fecha de regreso:</label>
						<input type="date" name="checkInDate" class="form-control">
					</div>
					<div class="form-group float-label-control">
						<label for="">Hora de regreso:</label>
						<input type="time" name="checkInTime" class="form-control">
					</div>
					<div class="form-group float-label-control">
						<label for="">Transporte:</label>
						<select class="form-control" name="transportation">
							<option selected>Seleccione una opción</option>
							<option value="Oficial">Oficial</option>
							<option value="Colectivo">Colectivo</option>
							<option value="Avión">Avión</option>
							<option value="Movilización">Movilización</option>
						</select>
					</div>
					<div>
						<input type="hidden" name="requestTypeId" value="Viatico">
					</div>
					<div class="form-group float-label-control">
						<button class="btn btn-primary" type="submit">Enviar solicitud</button>
					</div>
				</form>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							Otras Solicitudes
						</h3>
					</div>
					<div class="panel-body">
						<ul>
							<li><a href="/create/pagoServProf">Servicio Profesional</a></li>
							<li><a href="/create/sueldosDevengados">Sueldos Devengados</a></li>
							<li><a href="/create/certificacionHorario">Certificación de Horario</a></li>
							<li><a href="/create/certificacionTrabajo">Certificación de Trabajo</a></li>
							<li><a href="/create/devolucion">Devolución por Descuento</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('script')
	<script>
		document.getElementById("loc").addEventListener("click", removeInternationalFields);
		document.getElementById("int").addEventListener("click", displayInternationalFields);

		function displayInternationalFields() {
			document.getElementById("divPermisoPresi").innerHTML = "<hr><label for=''>Permiso de la Presidencia de la Republica de Panamá:</label><input type='file' accept='mimes:jpeg,bmp,png,gif,svg,pdf'name='permisoPresidencia' id='' class='form-control'>";
			document.getElementById("divProgramaEvento").innerHTML = "<label for=''>Programa del Evento:</label><input accept='mimes:jpeg,bmp,png,gif,svg,pdf' type='file' name='programaEvento' id='' class='form-control'>";
			document.getElementById("divRubrosOrganización").innerHTML = "<label for=''>Nota que emite la Organización que oficia el evento donde debe indicar qué rubros patrocina o cubre:</label><input accept='mimes:jpeg,bmp,png,gif,svg,pdf' type='file' name='rubrosOrganizacion' id='' class='form-control'>";
			document.getElementById("divAprobaciónRectoria").innerHTML = "<label for=''>Nota de aprobación de la Rectoría:</label><input accept='mimes:jpeg,bmp,png,gif,svg,pdf' type='file' name='aprobacionRectoria' id='' class='form-control'><hr>";
		}

		function removeInternationalFields() { 
			document.getElementById("divPermisoPresi").innerHTML = "";
			document.getElementById("divProgramaEvento").innerHTML = ""; 
			document.getElementById("divRubrosOrganización").innerHTML = ""; 
			document.getElementById("divAprobaciónRectoria").innerHTML = "";
		}
	</script>
@endsection
