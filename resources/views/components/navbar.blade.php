<nav class="navbar navbar-expand-xl navbar-dark bg-dark">
	  <a class="navbar-brand" href="#">AdmonUTP</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav">
			<li class="nav-item active">
				<a class="nav-link" href="#">Página Principal <span class="sr-only">(current)</span></a>
			</li>
			@auth
				<li class="dropdown nav-item">
					<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-expanded="false">
						Ver Solicitudes <span class="caret"></span>
					</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/my/requests" class="nav-link" style="color:black; text-decoration:none">Mis Solicitudes</a></li>
						@if (Auth::user()->roles()->where('user_role_id','<>', 2)->count())
							<li><a href="/assigned/requests" class="nav-link" style="color:black; text-decoration:none">Solicitudes Asignadas</a></li>
						@endif
					</ul>
				</li>
				@if (Auth::user()->roles()->where('user_role_id', 2)->count())
					<li class="nav-item dropdown">
							<a href="#" class="nav-link dropdown-toggle" role="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
								Crear Solicitudes <span class="caret"></span>
							</a>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton" role="menu">
								<a class="dropdown-item" href="/viaticos/create">Viaticos</a>
								<a class="dropdown-item" href="/cert-horarios/create">Certificación de Horarios</a>
								<a class="dropdown-item" href="/cert-sueldos-devengados/create">Certificación de Sueldos Devengados</a>
								<a class="dropdown-item" href="/cert-trabajo/create">Certificación de Trabajo</a>
								<a class="dropdown-item" href="/pago-serv-pro/create">Pago por Servicios Profesionales</a>
								<a class="dropdown-item" href="/devol-desc-mas/create">Devolución por descuento de Más</a>
							</div>
					</li>
				@endif
			@endauth
		</ul>
		<ul class="navbar-nav ml-auto">
			@auth
				<li class="dropdown nav-item">
					<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-expanded="false">
						{{Auth::user()->full_name}} <span class="caret"></span>
					</a>
					<ul class="dropdown-menu" role="menu">
						<a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" 
						style="color:black; text-decoration:none">Cerrar Sesión</a>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" 
						style="display: none;">{{ csrf_field() }}</form>
						<!--data-target="#myModal" data-toggle="modal"-->
					</ul>
				</li>
			@endauth
			@guest
				<li class="nav-item">
					<a class="nav-link" href="/login" >Iniciar Sesión</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/register">Registrarse</a>
				</li>
			@endguest

		</ul>
	</div>
</nav>
