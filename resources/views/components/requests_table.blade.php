@php
//defaults
$showSolicitante = $showSolicitante ?? true;
$showActions = $showActions ?? true;
$showCedula = $showCedula ?? true;
@endphp
@isset($requests)
<table class="table">
	<thead>
		<tr>
			<th>Id de Solicitud</th>
			<th>Tipo de Solicitud</th>
			@if($showSolicitante)<th>Solicitante</th>@endif
			@if($showCedula)<th>Cédula</th>@endif
			<th>Estado de la Solicitud</th>
			@if($showActions)<th>Acciones</th>@endif
		</tr>
	</thead>
	@foreach($requests as $request)
		<tr>
			<td>{{$request->id}}</td>
			<td>{{$request->type->name}} </td>
			@if($showSolicitante)<td>{{$request->requester->full_name}} </td>@endif
			@if($showCedula)<td>{{$request->requester->cedula}} </td>@endif
			<td>{{$request->status->name}} </td>
			@if($showActions)<td><a class="btn btn-primary" href="/request/{{$request->id}}">Ver</a></td>@endif
		</tr>
	@endforeach
</table>
@else
<p> No Content To Display Mate :( </p>
@endisset
