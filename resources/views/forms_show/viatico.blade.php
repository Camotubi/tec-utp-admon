@extends('layouts.app')
@section('content')
<div class="container">
    <div>
        <h2>Detalles de Solicitud de Viáticos</h2>
        <p class="lead">
            Certificación de servicio realizado y de recibido conforme<br />
            <small class="text-muted">Consulta sobre los viáticos en: <a href="http://www.utp.ac.pa/viaticos" target="blank">Universidad Tecnológica de Panamá - Viáticos</a></small>
        </p>

        <hr />

        <div class="row">
                    <div class="form-row">
                        <div class="col-sm-4 form-group float-label-control">
                            <label><strong>Nombre:</strong></label><p> {{ $body->name }}</p>
                        </div>
                        <div class="col-sm-4 form-group float-label-control">
                            <label><strong>Cédula:</strong></label><p> {{ $body->cedula }}</p>
                        </div>
                        <div class="col-sm-4 form-group float-label-control">
                            <label><strong>Cargo que desempeña:</strong></label><p> {{ $body->role }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-sm-4 form-group float-label-control">
                            <label for=""><strong>Lugar de Residencia:</strong></label> <p>{{ $body->home_address }}</p>
                        </div>
                        <div class="col-sm-4 form-group float-label-control">
                            <label for=""><strong>Observaciones adicionales:</strong></label> <p>{{ $body->add_observations }}</p>
                        </div>
                        <div class="col-sm-4 form-group float-label-control">
                            <label for=""><strong>Trabajo o misión a realizar:</strong></label> <p>{{ $body->objective }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-sm form-group float-label-control">
                            <label for=""><strong>Tipo de viaje:</strong></label> <p>{{ $body->viaticoType }}</p>
                        </div>
                        <div class="col-sm form-group float-label-control">
                            <label for=""><strong>Lugar de la misión:</strong></label> <p>{{ $body->place }}</p>
                        </div>
                        <div class="col-sm form-group float-label-control">
                            <label for=""><strong>Transporte:</strong></label> <p>{{ $body->transportation }}</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-sm form-group float-label-control">
                            <label class="label"><strong>Fecha de salida:</strong></label> <p>{{ $body->checkOutDate }}</p>
                        </div>
                        <div class="col-sm form-group float-label-control">
                            <label for=""><strong>Hora de salida:</strong></label> <p>{{ $body->checkOutTime }}</p>
                        </div>
                        <div class="col-sm form-group float-label-control">
				<label for=""><strong>Fecha de regreso:</strong></label> <p>{{ $body->checkInDate }}</p>
			</div>
			<div class="col-sm form-group float-label-control">
				<label for=""><strong>Hora de regreso:</strong></label> <p>{{ $body->checkInTime }}</p>
			</div>
		    </div>
	</div>
	@if($body->viaticoType !=="Local")
		<div class="form-group float-label-control">
			@isset($body->permisoPresidencia)
			<p><a href="/storage/{{$body->permisoPresidencia}}">Ver Permiso de la Presidencia de la Republica de Panamá</a></p>
		@endisset
		@isset($body->programaEvento)
		<p><a href="/storage/{{$body->programaEvento}}">Ver Programa del Evento</a></p>
	@endisset
	@isset($body->rubrosOrganizacion)
	<p><a href="/storage/{{$body->rubrosOrganizacion}}">Ver Nota que emite la Organización que oficia el evento donde debe indicar qué rubros patrocina o cubre</a></p>
@endisset
@isset($body->aprobacionRectoria)
<p><a href="/storage/{{$body->aprobacionRectoria}}">Ver Nota de aprobación de la Rectoría</a></p>
    @endisset
			</div>
		@endif
		            <div class="d-flex justify-content-center">
                            @if ($requestStatus == 1)
                                <div class="form-group float-label-control">
                                    @if (Auth::id() == $formRequest->requester_id)
                                    <h3>Su solicitud ha sido enviada y esta siendo evaluada</h3>
                                    @elseif (Auth::id() == $formRequest->incharge_id)
					    <form class="container-fluid"role="form" method="POST" action="/request/{{ $formRequest->id }}">
						    @method('PUT')
						    @csrf
						    <div class="form-group float-label-control">
							    <button class="btn btn-primary" type="submit" name="requestStatusId" value="2">Procesar</button>
				<a class="btn btn-danger" href="/deny/request/{{$formRequest->id}}" >Rechazar</a>
						    </div>
					    </form>
                                    @endif
				@elseif ($requestStatus == 2)
					<div class="form-group float-label-control">
						@if (Auth::id() == $formRequest->requester_id)
							<h3>Su solicitud fue aceptada y se esta procesando. Por favor esperar hasta que la persona responsable le de una respuesta.</h3>
						@elseif (Auth::id() == $formRequest->incharge_id)
							<h3>Insertar la información necesaria para responder a la solicitud</h3>
							<form class="container-fluid" enctype= "multipart/form-data" role="form" method="POST" action="/request/{{ $formRequest->id }}">
								@method('PUT')
								@csrf
								<div class="form-group">
									<label>Certificado / Documento Validador:</label>
									<input type="file" name="documento" id="" class="form-control">
								</div>
								<div class="form-group">
									<label>Instrucciones u observaciones adicionales:</label>
									<textarea class="form-control" name="obs" placeholder="Participación en el foro: UTP EMPRENDE en el hotel Hilton." rows="5"></textarea>
								</div>
								<input type="hidden" name="action" value="respond">
								<button class="btn btn-primary" type="submit" name="requestStatusId" value="4">Enviar</button>
							</form>
						@endif
					</div>
				@elseif ($requestStatus == 3)
					<div class="form-group float-label-control">
						<div class="alert alert-danger">
							<h3>Solicitud Rechazada</h4>
							<p><strong>Razon: </strong>{{$formRequest->response()->first()->obs}} </p>
						</div>
					</div>
				@elseif ($requestStatus == 4)
					<div class="form-group float-label-control">
						@if (Auth::id() == $formRequest->requester_id)
							<h3>Su solicitud fue aceptada. Esta es la respuesta de la persona responsable de su solicitud.</h3><br>
						@elseif (Auth::id() == $formRequest->incharge_id)
							<h3>Respuesta enviada:</h3><br>
						@endif
							<h4>Instrucciones u observaciones adicionales:</h4>
							<p>{{$formRequest->response()->first()->obs}} </p>
							<a href="/storage/{{$formRequest->response()->first()->documento}}" class="btn btn-primary">Descargar Certificado</a>
					</div>
				@endif
			</div>



		</div>
	</div>
	</div>
@endsection
