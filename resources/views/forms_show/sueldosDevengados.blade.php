@extends('layouts.app')
@section('content')
	<div class="container">
		<div>
			<h2>Detalles de Solicitud de Sueldos Devengados</h2>
			<p class="lead">
				Certificación de servicio realizado y de recibido conforme<br />
				<small class="text-muted">Consulta sobre los sueldos devengados en: <a href="http://www.utp.ac.pa/viaticos" target="blank">Universidad Tecnológica de Panamá - Devengados</a></small>
			</p>

			<hr />

			<div class="row">
				<div class="container-fluid">
					<div class="form-row">
						<div class="col-sm-4 form-group float-label-control">
							<label><strong>Nombre:</strong></label><p> {{ $formRequest->requester()->first()->full_name }}</p>
						</div>
						<div class="col-sm-4 form-group float-label-control">
							<label><strong>Cédula:</strong></label><p> {{ $formRequest->requester()->first()->cedula }}</p>
						</div>
					</div>
					<div class="form-row">
						<div class="col-sm-4 form-group float-label-control">
							<label for=""><strong>Fecha Inicio:</strong></label> <p>{{ $body->CheckInDate }}</p>
						</div>
						<div class="col-sm-4 form-group float-label-control">
							<label for=""><strong>Fecha Final:</strong></label> <p>{{ $body->CheckOutDate }}</p>
						</div>
					</div>
				</div> 
				<div class="container-fluid d-flex justify-content-center">
					@if ($requestStatus == 1)
						<div class="form-group float-label-control">
							@if (Auth::id() == $formRequest->requester_id)
								<h3>Su solicitud ha sido enviada y esta siendo evaluada</h3>
							@elseif (Auth::id() == $formRequest->incharge_id)
								<form class="container-fluid" role="form" method="POST" action="/request/{{ $formRequest->id }}" >
									@method('PUT')
									@csrf
									<div class="form-group float-label-control">
										<button class="btn btn-primary" type="submit" name="requestStatusId" value="2">Procesar</button>
										<a class="btn btn-danger" href="/deny/request/{{$formRequest->id}}" >Rechazar</a>

									</div>
								</form>
							@endif
						</div>
					@elseif ($requestStatus == 2)
						<div class="form-group float-label-control">
							@if (Auth::id() == $formRequest->requester_id)    
								<h3>Su solicitud fue aceptada y se esta procesando. Por favor esperar hasta que la persona responsable le de una respuesta.</h3>
							@elseif (Auth::id() == $formRequest->incharge_id)
							<h3>Insertar la información necesaria para responder a la solicitud</h3>
								<form class="container-fluid" enctype= "multipart/form-data" role="form" method="POST" action="/request/{{ $formRequest->id }}">
									@method('PUT')
									@csrf
									<div class="form-group">
										<label>Certificado / Documento Validador:</label>
										<input type="file" name="documento" id="" class="form-control">
									</div>
									<div class="form-group">
										<label>Instrucciones u observaciones adicionales:</label>
										<textarea class="form-control" name="obs" placeholder="Participación en el foro: UTP EMPRENDE en el hotel Hilton." rows="5"></textarea>
									</div>
									<input type="hidden" name="action" value="respond">
									<button class="btn btn-primary" type="submit" name="requestStatusId" value="4">Enviar</button>
								</form>
							@endif
						</div>
					@elseif ($requestStatus == 3)
						<div class="form-group float-label-control">
							<div class="alert alert-danger">
								<h3>Solicitud Rechazada</h4>
								<p><strong>Razon: </strong>{{$formRequest->response()->first()->obs}} </p>
							</div>
						</div>
					@elseif ($requestStatus == 4)
						<div class="form-group float-label-control">
							@if (Auth::id() == $formRequest->requester_id)
								<h3>Su solicitud fue aceptada. Esta es la respuesta de la persona responsable de su solicitud.</h3><br>
							@elseif (Auth::id() == $formRequest->incharge_id)
								<h3>Respuesta enviada:</h3><br>
							@endif
							<h4>Instrucciones u observaciones adicionales:</h4>
							<p>{{$formRequest->response()->first()->obs}} </p>
							<a href="/storage/{{$formRequest->response()->first()->documento}}" class="btn btn-primary">Descargar Certificado</a>
						</div>
					@endif
				</div>




			</div>
		</div>
	</div>
@endsection
