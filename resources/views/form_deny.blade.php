@extends('layouts.app')
@section('content')
	<form method="POST" action="/deny/request/{{$id}}">
		@csrf
		<div class="form-group">
		<label>Razon del Rechazo de la Solicitud:</label>
		<textarea class="form-control"  name="reason" rows="5"></textarea> 
		</div>
		<input type ="submit" class="btn btn-danger" value="Rechazar">
	</form>
@endsection
